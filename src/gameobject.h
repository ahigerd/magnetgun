#ifndef MG_GAMEOBJECT_H
#define MG_GAMEOBJECT_H

#include <wit/renderer.h>
#include <wit/geometry.h>
#include "physics.h"
class Script;
class ScriptState;

class GameObject : public wit::Renderer
{
friend class Physics;
public:
    enum Polarity { Neutral, Positive, Negative };

    virtual ~GameObject();

    b2Body* body;
    Polarity polarity;
    bool canPolarize, isAttracting;
    bool isPlayer, isShot;

    bool useThrust;
    float speed;
    void applyThrust();

    void setFrameScript(Script* script);
    void runFrameScript();

    void adjustPolarity(bool pole);
    wit::Point center() const;
    wit::Rect boundingRect() const;

protected:
    GameObject(float x, float y, float hw, float hh, bool bullet = false); // constructs a box fixture
    GameObject(float x, float y, bool bullet = false); // construct your own fixture

    float halfWidth, halfHeight;
    int polarityCooldown;

    Script* frameScript;
    ScriptState* frameScriptState;

private:
    void initBody(float x, float y, bool bullet);
    void initFixture(float hw, float hh);
};

#endif
