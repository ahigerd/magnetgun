#include "bullet.h"

Bullet::Bullet(float x, float y, bool polarity)
: GameObject(x, y, 2, 15, true), polarity(polarity), lastY(y)
{
    b2Fixture* fixture = body->GetFixtureList();
    b2Filter filter;
    filter.categoryBits = 0x0001;
    filter.maskBits = 0xFFFE;
    filter.groupIndex = 0;
    fixture->SetFilterData(filter);
}

Bullet::~Bullet()
{
}

void Bullet::render()
{
    wit::Rect rect = boundingRect();
    rect.y2 = lastY;
    if(rect.y1 < 6) {
        Physics::instance()->queueDestroy(body);
    } else if(polarity) {
        fillRect(rect, wit::Color(255, 0, 0, 255));
        drawRect(rect, wit::Color(128, 0, 0, 255));
    } else {
        fillRect(rect, wit::Color(0, 0, 255, 255));
        drawRect(rect, wit::Color(0, 0, 128, 255));
    }
    lastY = rect.y1;
}
