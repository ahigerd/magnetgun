#include "physics.h"
#include "gameobject.h"
#include "bullet.h"
#include "script.h"
#include <wit/application.h>
#include <wit/renderer.h>

#define ATTRACTIVE_FORCE 200.0

static Physics* physicsInstance = 0;
const float32 timeStep = 1.0f / 60.0f;
const int velIter = 6, posIter = 2;

Physics* Physics::instance()
{
    return physicsInstance;
}

Physics::Physics() : world(b2Vec2(0.0f, 0.0f), false)
{
    physicsInstance = this;
}

void Physics::timerTriggered(void* data)
{
    // First, clean out any destroyed objects
    while(!destroyQueue.empty()) {
        GameObject* gameObject = reinterpret_cast<GameObject*>(destroyQueue.front()->GetUserData());
        if(gameObject) delete gameObject;
        destroyQueue.pop();
    }

    // Second, find center and strength of attractive forces
    // Do per-frame updates while we're in the loop
    b2Vec2 redBarycenter(0,0), blueBarycenter(0,0);
    float redMass = 0, blueMass = 0;
    b2Body* body = world.GetBodyList();
    while(body) {
        GameObject* gameObject = reinterpret_cast<GameObject*>(body->GetUserData());
        if(gameObject->polarityCooldown > 0) gameObject->polarityCooldown--;
        if(gameObject->polarity != GameObject::Neutral) {
            b2Vec2 pos = body->GetPosition();
            float mass = body->GetMass();
            pos.x *= mass;
            pos.y *= mass;
            if(gameObject->polarity == GameObject::Positive) {
                redMass += mass;
                redBarycenter += pos;
            } else if(gameObject->polarity == GameObject::Negative) {
                blueMass += mass;
                blueBarycenter += pos;
            }
        }
        body = body->GetNext();
    }
    if(redMass > b2_epsilon) redBarycenter *= 1.0 / redMass;
    if(blueMass > b2_epsilon) blueBarycenter *= 1.0 / blueMass;

    // Third, apply forces
    body = world.GetBodyList();
    if(redMass > b2_epsilon && blueMass >= b2_epsilon) {
        while(body) {
            GameObject* gameObject = reinterpret_cast<GameObject*>(body->GetUserData());
            if(gameObject->polarity != GameObject::Neutral) {
                b2Vec2 pos = body->GetPosition();
                b2Vec2 vector;
                if(gameObject->polarity == GameObject::Positive) {
                    vector = (blueBarycenter - pos);
                } else if(gameObject->polarity == GameObject::Negative) {
                    vector = (redBarycenter - pos);
                }
                float length = vector.Normalize();
                if(length > b2_epsilon) {
                    if(!gameObject->isAttracting) {
                        // Zero out the velocity not along the vector
                        b2Vec2 vel = gameObject->body->GetLinearVelocity();
                        if(vel.LengthSquared() > b2_epsilon) {
                            float scalar = b2Dot(vel, vector) / vector.LengthSquared();
                            b2Vec2 newVel = vector;
                            newVel *= scalar;
                            gameObject->body->SetLinearVelocity(newVel);
                        }
                    }
                    length = ATTRACTIVE_FORCE / length;
                    vector *= length;
                    body->ApplyForceToCenter(vector);
                } else if(!gameObject->isAttracting) {
                    // Already at the barycenter, so zero the velocity
                    gameObject->body->SetLinearVelocity(b2Vec2(0.0, 0.0));
                }
                gameObject->isAttracting = true;
            } else {
                gameObject->isAttracting = false;
                gameObject->runFrameScript();
                gameObject->applyThrust();
            }
            body = body->GetNext();
        }
    } else {
        while(body) {
            GameObject* gameObject = reinterpret_cast<GameObject*>(body->GetUserData());
            gameObject->isAttracting = false;
            gameObject->runFrameScript();
            gameObject->applyThrust();
            body = body->GetNext();
        }
    }

    // Fourth, update physics
    world.Step(timeStep, velIter, posIter);
    world.ClearForces();

    // Fifth, deal with collisions
    b2Contact* contact = world.GetContactList();
    while(contact) {
        b2Body* body1 = contact->GetFixtureA()->GetBody();
        b2Body* body2 = contact->GetFixtureB()->GetBody();
        GameObject* obj1 = reinterpret_cast<GameObject*>(body1->GetUserData());
        GameObject* obj2 = reinterpret_cast<GameObject*>(body2->GetUserData());
        if(obj1 == obj2) {
            contact = contact->GetNext();
            continue;
        }

        if((obj1->isShot || obj2->isShot) && !obj1->isPlayer && !obj2->isPlayer) {
            if(obj1->isShot && obj2->canPolarize) {
                Bullet* shot = static_cast<Bullet*>(obj1);
                obj2->adjustPolarity(shot->polarity);
                queueDestroy(body1);
            } else if(obj2->isShot && obj1->canPolarize) {
                Bullet* shot = static_cast<Bullet*>(obj2);
                obj1->adjustPolarity(shot->polarity);
                queueDestroy(body2);
            }
        } else {
            if(obj1->polarity == GameObject::Neutral || obj2->polarity == GameObject::Neutral || obj1->polarity == obj2->polarity) {
                // only oppositely-charged enemies can collide
                contact = contact->GetNext();
                continue;
            }
            // collision!
            queueDestroy(body1);
            queueDestroy(body2);
        }
        contact = contact->GetNext();
    }

    // Finally, set up for the next tick
    witApp()->scheduleTimer(0, this, 0);
}

void Physics::queueDestroy(b2Body* body)
{
    destroyQueue.push(body);
}
