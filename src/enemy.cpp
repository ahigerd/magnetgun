#include "enemy.h"
#include <wit/wit.h>
#include <wit/event.h>
#include <wit/application.h>

#define SHIP_FORCE 120 

Enemy::Enemy(float x, float y, float hw, float hh) 
: GameObject(x, y, hw, hh)
{
    b2Fixture* fixture = body->GetFixtureList();
    b2Filter filter;
    filter.categoryBits = 0x0002;
    filter.maskBits = 0xFFFF; 
    filter.groupIndex = 0;
    fixture->SetFilterData(filter);
}

void Enemy::render()
{
    wit::Polygon poly;
    b2Vec2 point = body->GetWorldPoint(b2Vec2(0.5, 0.5));
    poly.addPoint(point.x * 32.0, point.y * 32.0);
    point = body->GetWorldPoint(b2Vec2(-0.5, 0.5));
    poly.addPoint(point.x * 32.0, point.y * 32.0);
    point = body->GetWorldPoint(b2Vec2(-0.5, -0.5));
    poly.addPoint(point.x * 32.0, point.y * 32.0);
    point = body->GetWorldPoint(b2Vec2(0.5, -0.5));
    poly.addPoint(point.x * 32.0, point.y * 32.0);
    if(polarity == Positive) {
        fillPolygon(poly, wit::Color(255,0,0,255));
    } else if(polarity == Negative) {
        fillPolygon(poly, wit::Color(0,0,255,255));
    }
    drawPolygon(poly, wit::Color(255,255,255,255));
}

