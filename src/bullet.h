#ifndef MG_BULLET_H
#define MG_BULLET_H

#include "gameobject.h"

class Bullet : public GameObject
{
public:
    Bullet(float x, float y, bool polarity);
    ~Bullet();

    void render();

    bool polarity;
    float lastY;
};

#endif
