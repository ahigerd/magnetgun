#ifndef MG_SHIP_H
#define MG_SHIP_H

#include "gameobject.h"
#include <wit/eventhandler.h>

class Ship : public GameObject, public wit::EventHandler
{
public:
    Ship();

    bool buttonPressEvent(wit::ButtonEvent* event);
    bool buttonReleaseEvent(wit::ButtonEvent* event);
    void render();

private:
    void shoot(bool polarity, bool renderNow = false);

    int fx, fy;
    int fireCooldown;
    bool b1Down, b2Down, currShot;
};

#endif
