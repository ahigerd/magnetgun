#ifndef MG_PHYSICS_H
#define MG_PHYSICS_H

#include <wit/eventhandler.h>
#include <Box2D/Box2D.h>
#include <queue>

class Physics : public wit::EventHandler
{
public:
    static Physics* instance();
    Physics();

    void timerTriggered(void* data);
    void queueDestroy(b2Body* body);

    b2World world;

private:
    std::queue<b2Body*> destroyQueue;

};

#endif
