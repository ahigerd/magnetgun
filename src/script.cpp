#include "script.h"
#include "gameobject.h"
#include <QtDebug>

static float RAD2DEG = 57.2957795;

static int getLabel(std::vector<std::string>& labelNames, const std::string& label)
{
    int ct = labelNames.size();
    for(int i = 0; i < ct; i++) {
        if(labelNames[i] == label)
            return i;
    }
    labelNames.push_back(label);
    return ct;
}

static int getVariable(std::vector<std::string>& variableNames, const std::string& variable)
{
    // First check for special variables that mean something
    if(variable == "x")         return -1;
    if(variable == "y")         return -2;
    if(variable == "speed")     return -3;
    if(variable == "angle")     return -4;
    if(variable == "thrust")    return -5;

    // Otherwise loop over the defined variables
    int ct = variableNames.size();
    for(int i = 0; i < ct; i++) {
        if(variableNames[i] == variable)
            return i;
    }

    // If one isn't found, add it
    variableNames.push_back(variable);
    return ct;
}

Script::Script(std::istream& stream)  
{
    std::vector<std::string> labelNames;
    std::vector<std::string> variableNames;
    std::string token;
    ScriptFunction fn;

    while(!stream.eof()) {
        stream >> token;
        if(token == "vsync") {
            fn.function = ScriptFunction::VSync;
        } else if(token == "goto") {
            fn.function = ScriptFunction::Goto;
            std::string label;
            stream >> label;
            fn.int1 = getLabel(labelNames, label);
        } else if(token == "inc") {
            fn.function = ScriptFunction::Increment;
            std::string var;
            stream >> var;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "dec") {
            fn.function = ScriptFunction::Decrement;
            std::string var;
            stream >> var;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "sprite") {
            fn.function = ScriptFunction::SetSprite;
            stream >> fn.stringParam;
        } else if(token == "set") {
            fn.function = ScriptFunction::SetNumber;
            std::string var;
            stream >> var >> fn.param2;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "ifgreater") {
            fn.function = ScriptFunction::IfGreater; 
            std::string var;
            stream >> var >> fn.param2;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "ifless") {
            fn.function = ScriptFunction::IfLess; 
            std::string var;
            stream >> var >> fn.param2;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "ifgreater2") {
            fn.function = ScriptFunction::IfGreaterVar; 
            std::string var, var2;
            stream >> var >> var2;
            fn.int1 = getVariable(variableNames, var);
            fn.int2 = getVariable(variableNames, var);
        } else if(token == "ifless2") {
            fn.function = ScriptFunction::IfLessVar; 
            std::string var, var2;
            stream >> var >> var2;
            fn.int1 = getVariable(variableNames, var);
            fn.int2 = getVariable(variableNames, var);
        } else if(token == "aim") {
            fn.function = ScriptFunction::AimAtObject;
            stream >> fn.stringParam;
        } else if(token == "add") {
            fn.function = ScriptFunction::Addition;
            std::string var;
            stream >> var >> fn.param2;
            fn.int1 = getVariable(variableNames, var);
        } else if(token == "/*") {
            do {
                stream >> token;
            } while(token != "*/");
        } else if(token[0] == ':') {
            std::string label = token.substr(1);
            int labelID = getLabel(labelNames, label);
            while(labels.size() <= labelID)
                labels.push_back(-1);
            labels[labelID] = ops.size();
            fn.function = ScriptFunction::Unknown;
        } else {
            fn.function = ScriptFunction::Unknown;
        }
        ops.push_back(fn);
    }
    numVars = variableNames.size();
    scriptEnd = ops.size();
}

void Script::runToVSync(ScriptState* state)
{
    while(state->lineNumber < scriptEnd) {
        ScriptFunction& fn = ops[state->lineNumber];
        state->lineNumber++;
        switch(fn.function) {
            case ScriptFunction::VSync:
                return;
            case ScriptFunction::Goto:
                if(labels[fn.int1] != -1)
                    state->lineNumber = labels[fn.int1];
                break;
            case ScriptFunction::Increment:
                state->setValue(fn.int1, state->getValue(fn.int1) + 1);
                break;
            case ScriptFunction::Decrement:
                state->setValue(fn.int1, state->getValue(fn.int1) - 1);
                break;
            case ScriptFunction::SetSprite:
                // todo
                break;
            case ScriptFunction::AimAtObject:
                // todo
                break;
            case ScriptFunction::SetNumber:
                state->setValue(fn.int1, fn.param2);
                break;
            case ScriptFunction::IfGreater:
                if(state->getValue(fn.int1) <= fn.param2)
                    state->lineNumber++;
                break;
            case ScriptFunction::IfLess:
                if(state->getValue(fn.int1) >= fn.param2)
                    state->lineNumber++;
                break;
            case ScriptFunction::IfGreaterVar:
                if(state->getValue(fn.int1) <= state->getValue(fn.int2))
                    state->lineNumber++;
                break;
            case ScriptFunction::IfLessVar:
                if(state->getValue(fn.int1) >= state->getValue(fn.int2))
                    state->lineNumber++;
                break;
            case ScriptFunction::Addition:
                state->setValue(fn.int1, state->getValue(fn.int1) + fn.param2);
                break;
            default:
                ;
        }
    }
}

ScriptState::ScriptState(GameObject* object, Script* script)
: object(object), lineNumber(0), floatVars(script->numVars, 0.0)
{
    // initializers only
}

float ScriptState::getValue(int variable) const
{
    if(variable >= 0) {
        // script variable, ignore object
        return floatVars[variable];
    } else if(variable == -1 /* x */) {
        return object->body->GetPosition().x * 32.0;
    } else if(variable == -2 /* y */) {
        return object->body->GetPosition().y * 32.0;
    } else if(variable == -3 /* speed */) {
        return object->speed;
    } else if(variable == -4 /* angle */) {
        return object->body->GetAngle() * RAD2DEG;
    } else if(variable == -5 /* thrust */) {
        return object->useThrust ? 1.0 : 0.0;
    } else {
        return 0.0;
    }
}

void ScriptState::setValue(int variable, float value)
{
    if(variable >= 0) {
        // script variable, ignore object
        floatVars[variable] = value;
    } else if(variable == -1 /* x */) {
        b2Transform pos = object->body->GetTransform();
        pos.p.x = value / 32.0;
        object->body->SetTransform(pos.p, pos.q.GetAngle());
    } else if(variable == -2 /* y */) {
        b2Transform pos = object->body->GetTransform();
        pos.p.y = value / 32.0;
        object->body->SetTransform(pos.p, pos.q.GetAngle());
    } else if(variable == -3 /* speed */) {
        object->speed = value;
    } else if(variable == -4 /* angle */) {
        object->body->SetTransform(object->body->GetTransform().p, value / RAD2DEG);
    } else if(variable == -5 /* thrust */) {
        object->useThrust = (value > 0.5) || (value < -0.5);
    }
}
