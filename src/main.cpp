#include <wit/wit.h>
#include <wit/application.h>
#include "ship.h"
#include "enemy.h"
#include "physics.h"
#include "script.h"
#include <fstream>
#include <QFile>

int main(int argc, char** argv)
{
    Physics physics;
    wit::Application app(wit::UseGX | wit::UseWiiRemote | wit::UseDoubleBuffer | wit::UseGameCube);
    app.setRemoteHorizontal(0, true);
#ifdef WIT_USE_QT
    app.setAutoHomeMenu(true);
#endif

    Ship ship;
    app.installEventFilter(&ship);

    std::ifstream scriptFile("../scripts/demo.txt");
    Script* script = new Script(scriptFile);

    Enemy *e1 = new Enemy(100, 100, 16, 16), *e2 = new Enemy(500, 200, 16, 16);
    Enemy *e3 = new Enemy(200, 300, 16, 16);
    e1->canPolarize = e2->canPolarize = e3->canPolarize = true;
    e2->setFrameScript(script);

    witApp()->scheduleTimer(0, &physics, 0);
    app.run();
    return 0;
}
