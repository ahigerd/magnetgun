#include "gameobject.h"
#include "script.h"

GameObject::GameObject(float x, float y, float hw, float hh, bool bullet) : wit::Renderer(), halfWidth(hw), halfHeight(hh)
{
    initBody(x, y, bullet);
    initFixture(hw, hh);
}

GameObject::GameObject(float x, float y, bool bullet) : wit::Renderer(), halfWidth(0), halfHeight(0)
{
    initBody(x, y, bullet);
} 

GameObject::~GameObject()
{
    body->SetUserData(0);
    Physics::instance()->world.DestroyBody(body);
}

void GameObject::initBody(float x, float y, bool bullet)
{
    frameScript = 0;
    frameScriptState = 0;
    speed = 0.0;
    useThrust = false;
    polarity = Neutral;
    canPolarize = false;
    polarityCooldown = 0;
    isAttracting = false;
    isPlayer = false;
    isShot = bullet;

    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(x / 32.0, y / 32.0);
    bodyDef.userData = this;
    bodyDef.fixedRotation = true;
    bodyDef.bullet = bullet;
    body = Physics::instance()->world.CreateBody(&bodyDef);
}

void GameObject::initFixture(float hw, float hh)
{
    b2PolygonShape shapeDef;
    shapeDef.SetAsBox(hw / 32.0, hh / 32.0);

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &shapeDef;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.isSensor = true; // we use sensors because nothing in this game should push anything else
    body->CreateFixture(&fixtureDef);
}

wit::Point GameObject::center() const
{
    b2Vec2 pos = body->GetPosition();
    return wit::Point(pos.x * 32.0, pos.y * 32.0);
}

wit::Rect GameObject::boundingRect() const
{
    wit::Point c = center();
    return wit::Rect(c.x - halfWidth, c.y - halfHeight, c.x + halfWidth, c.y + halfHeight);
}

void GameObject::adjustPolarity(bool pole)
{
    if(!canPolarize || polarityCooldown > 0) return;
    polarityCooldown = 12;
    if(pole) {
        if(polarity == Negative)
            polarity = Neutral;
        else
            polarity = Positive;
    } else {
        if(polarity == Positive)
            polarity = Neutral;
        else
            polarity = Negative;
    }
}

void GameObject::applyThrust()
{
    if(!useThrust || isAttracting) return;
    b2Rot rot = body->GetTransform().q;
    body->SetLinearVelocity(b2Vec2(speed * rot.c, speed * rot.s));
}

void GameObject::setFrameScript(Script* script)
{
    delete frameScriptState;
    if(!script) {
        frameScript = 0;
        frameScriptState = 0;
    } else {
        frameScript = script;
        frameScriptState = new ScriptState(this, script);
    }
}

void GameObject::runFrameScript()
{
    if(!frameScript) return;
    frameScript->runToVSync(frameScriptState);
}
