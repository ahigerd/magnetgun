INCLUDEPATH += ../../wit .. .
DEFINES += WIT_USE_QT
win32 {
  LIBS += ../../wit/wit/wit.lib ../qt/Box2D.lib
  PRE_TARGETDEPS += ../../wit/wit/wit.lib ../qt/Box2D.lib
}
else {
  LIBS += ../../wit/wit/libwit.a ../qt/libBox2D.a
  PRE_TARGETDEPS += ../../wit/wit/libwit.a ../qt/libBox2D.a
}
DESTDIR = ../qt
TARGET = magnetgun
OBJECTS_DIR = ../qt

HEADERS += gameobject.h ship.h physics.h bullet.h enemy.h script.h
SOURCES += gameobject.cpp ship.cpp physics.cpp bullet.cpp enemy.cpp script.cpp main.cpp
