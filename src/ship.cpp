#include "ship.h"
#include "bullet.h"
#include <wit/wit.h>
#include <wit/event.h>
#include <wit/application.h>

#define SHIP_FORCE 120 

Ship::Ship() 
: GameObject(witApp()->screenWidth() / 2, 432, 16, 16),
  fx(0), fy(0), fireCooldown(0), b1Down(false), b2Down(false)
{
    body->SetLinearDamping(10.0f);
    isPlayer = true;

    b2Fixture* fixture = body->GetFixtureList();
    b2Filter filter;
    filter.categoryBits = 4;
    filter.maskBits = 0xFFFF & ~1; // doesn't collide with player bullets
    fixture->SetFilterData(filter);
}

void Ship::shoot(bool polarity, bool renderNow)
{
    if(fireCooldown == 0) {
        wit::Point pos = center();
        Bullet* bullet = new Bullet(pos.x, pos.y - 17, polarity);
        bullet->body->ApplyForceToCenter(b2Vec2(0, -200)); 
        fireCooldown = 12;
        if(renderNow) bullet->render();
    }
    currShot = polarity;
}

bool Ship::buttonPressEvent(wit::ButtonEvent* event)
{
    if(event->button == wit::Button_Home) {
        witApp()->quit();
    } else if(event->button == wit::Button_Left) {
        fx -= SHIP_FORCE;
    } else if(event->button == wit::Button_Right) {
        fx += SHIP_FORCE;
    } else if(event->button == wit::Button_Up) {
        fy -= SHIP_FORCE;
    } else if(event->button == wit::Button_Down) {
        fy += SHIP_FORCE;
    } else if(event->button == wit::Button_1 || event->button == wit::Button_2 || (event->controllerType != wit::WiiRemote && (event->button == wit::Button_A || event->button == wit::Button_B))) {
        shoot(event->button == wit::Button_1 || event->button == wit::Button_B);
        if(event->button == wit::Button_1 || event->button == wit::Button_B) {
            b1Down = true;
        } else {
            b2Down = true;
        }
    }
    return true;
}

bool Ship::buttonReleaseEvent(wit::ButtonEvent* event)
{
    if(event->button == wit::Button_Left) {
        fx += SHIP_FORCE;
    } else if(event->button == wit::Button_Right) {
        fx -= SHIP_FORCE;
    } else if(event->button == wit::Button_Up) {
        fy += SHIP_FORCE;
    } else if(event->button == wit::Button_Down) {
        fy -= SHIP_FORCE;
    } else if(event->button == wit::Button_1 || (event->controllerType != wit::WiiRemote && event->button == wit::Button_B)) {
        b1Down = false;
        if(b2Down) currShot = false;
    } else if(event->button == wit::Button_2 || (event->controllerType != wit::WiiRemote && event->button == wit::Button_A)) {
        b2Down = false;
        if(b1Down) currShot = true;
    }
    return true;
}

void Ship::render()
{
    if(fx < -SHIP_FORCE) fx = -SHIP_FORCE;
    else if(fx > SHIP_FORCE) fx = SHIP_FORCE;
    if(fy < -SHIP_FORCE) fy = -SHIP_FORCE;
    else if(fy > SHIP_FORCE) fy = SHIP_FORCE;
    body->ApplyForceToCenter(b2Vec2(fx, fy));
    drawRect(boundingRect(), wit::Color(255,255,255,255));
    if(fireCooldown > 0) {
        fireCooldown--;
    } else if(b1Down || b2Down) {
        shoot(currShot, true);
    }
}
