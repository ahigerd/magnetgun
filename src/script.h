#ifndef MG_SCRIPT_H
#define MG_SCRIPT_H

#include <string>
#include <vector>
#include <iostream>
class GameObject;

struct ScriptFunction {
    enum Function {
        // zero-parameter functions
        VSync,
        Unknown,
        // one-parameter functions
        Goto,
        Increment,
        Decrement,
        SetSprite,
        AimAtObject,
        // two-parameter functions
        SetNumber,
        IfGreater,
        IfLess,
        IfGreaterVar,
        IfLessVar,
        Addition
    };
    Function function;
    union {
        float param1;
        int int1;
    };
    union {
        float param2;
        int int2;
    };
    std::string stringParam;
};

class Script;
class ScriptState {
friend class Script;
public:
    ScriptState(GameObject* object, Script* script);

    float getValue(int variable) const;
    void setValue(int variable, float value);

private:
    GameObject* object;
    int lineNumber;
    std::vector<float> floatVars;
};

class Script {
friend class ScriptState;
public:
    Script(std::istream& stream);

    void runToVSync(ScriptState* state);

private:
    int scriptEnd, numVars;
    std::vector<int> labels;
    std::vector<ScriptFunction> ops;

};


#endif
