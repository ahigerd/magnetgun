#ifndef MG_ENEMY_H
#define MG_ENEMY_H

#include "gameobject.h"

class Enemy : public GameObject
{
public:
    Enemy(float x, float y, float hw, float hh);

    void render();
};

#endif

